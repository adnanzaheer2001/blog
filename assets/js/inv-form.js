	//Add Party Name with Value in Combo Box
	$(document).ready(function() {
		$('#txtpartyname').select2();
	});
	//Add item Name with Value in Combo Box
	$(document).ready(function() {
		$('#txtitemname').select2();
	});
	//Amount=Qty x rate
	$('#txtrate').on('focusout',function(){
		$('#txtamount').val(parseInt($('#txtrate').val())*parseInt($('#txtqty').val()));
	});
	// Focus on txtamount will shift to Save Button	
	$('#txtamount').on('focus', function () { 
		$('#btnsave').focus();
	});
	//	On click save button
	$('#btnsave').on('click',function(e){
		var $temp_a = $('#txtamount').val();
		if(parseInt(($temp_a))==0){
			$('#txtitemname').focus();	
			return false;
		}
		//generate serial number
		var arr=[];
		var aa=0;
		$("td:first-child").each(function(){
			arr.push($(this).text());
		});
		$.each(arr,function(index,value){
			if (aa<arr[index]){aa=parseInt(arr[index])+1;}
			if (aa==0){aa=1;} 
		});
		$('#txtamount').val(parseInt($('#txtrate').val())*parseInt($('#txtqty').val()));
		if(isNaN($('#txtamount').val())||isNaN($('#txtqty').val())||isNaN($('#txtrate').val())||($('#txtamount').val())==0){
			$('#txtqty').focus();
			return false;
		}
		$("#tblinvitems").append('<tr><td id="td_sr" style="text-align:center">'+aa+'</td><td id="td_itemname">'+$('#txtitemname option:selected').text()+'</td><td id="td_qty" style="text-align:right">'+parseInt( $('#txtqty').val())+'</td><td id="td_rate" style="text-align:right">'+parseInt($('#txtrate').val())+'</td><td id="td_amount" style="text-align:right">'+$('#txtamount').val()+'<td id="td_action" style="text-align:center"><button id="btndelete" >Delete</button></td></tr>');		
		//count rows
		$('#p_totalrows').text(($('#tblinvitems >tr').length));
		//return null value
		$('#txtqty').val(0);
		$('#txtrate').val(0);
		$('#txtamount').val(0);
		//add Total of spacific colums 
		$('#p_totalamount').text(0);
		var aa1=0;
		$('td:nth-child(5)').each(function(){  
			aa1=aa1+parseInt($(this).text());
		});
		$('#p_totalamount').text(aa1);
		//********************************/
		// //total qty column
		$('#p_totalqty').text(0);
		aa1=0;
		$('td:nth-child(3)').each(function(){  
			aa1=aa1+parseInt($(this).text());
		});
		$('#p_totalqty').text(aa1);
		//********************************/
		$('#txtitemname').focus();
		e.preventDefault();
	});				
	//on press cancel button
	$('#btncancel').on('click', function (e) {
		$('#txtqty').val(0);
		$('#txtrate').val(0);
		$('#txtamount').val(0);
		$('#txtitemname').focus();
		e.preventDefault();
	});
	//on press delet button
	$("#tblinvitems").on('click', '#btndelete', function () {
		$(this).closest('tr').remove();
		// //total amount column
		$('#p_totalamount').text(0);
		var aa1=0;
		$('td:nth-child(5)').each(function(){  
			aa1=aa1+parseInt($(this).text());
		});
		$('#p_totalamount').text(aa1);
		//********************************/
		// //total qty column
		$('#p_totalqty').text(0);
		 aa1=0;
		$('td:nth-child(3)').each(function(){  
			aa1=aa1+parseInt($(this).text());
		});
		$('#p_totalqty').text(aa1);
		//********************************/
		//total rows
		$('#p_totalrows').text(($('#tblinvitems >tr').length));
		//******************************* */
		$('#txtitemname').focus();
	});	
	