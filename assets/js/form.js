//add course on change of program
$( document ).ready( function(){
    $('#txtprogram').on('change', function(){
        if ( this.value == "MBA"   ) { 
            $("#chkbox").empty(); //For deleting all child objects.
            $("#chkbox").append('<input type="checkbox" name="cs101" id="cs101" />').append('<label for="cs101">CS-101 Introduction to Computing</label><br>');
            $("#chkbox").append('<input type="checkbox" name="mcm301" id="mcm301" />').append('<label for="mcm301">MCM301 - Communication skills</label><br>');
            $("#chkbox").append('<input type="checkbox" name="mgt101" id="mgt101" />').append('<label for="mgt101">MGT101 - Financial Accounting</label><br>');
            $("#chkbox").append('<input type="checkbox" name="mgt211" id="mgt211" />').append('<label for="mgt211">mgt211 - Introduction To Business</label><br>');
            $("#chkbox").append('<input type="checkbox" name="mth302" id="mth302" />').append('<label for="mth302">MTH302 - Business Mathematics & Statistics</label><br>');
            $("#chkbox").css("margin-top", "10px");
        } 
        else if (this.value=="MIT") {
            $("#chkbox").empty(); //For deleting all child objects.
            $("#chkbox").append('<input type="checkbox" name="cs201" id="cs201" />').append('<label for="cs201">CS-201  Introduction to Programming</label><br>');
            $("#chkbox").append('<input type="checkbox" name="mgt101" id="mgt101" />').append('<label for="mgt101">MGT101 - Financial Accounting</label><br>');
            $("#chkbox").append('<input type="checkbox" name="cs601" id="cs601" />').append('<label for="cs601">CS601 - Data Communication</label><br>');
            $("#chkbox").append('<input type="checkbox" name="eng201" id="eng201" />').append('<label for="eng201">ENG201 - Business and Technical English Writing</label><br>');
            $("#chkbox").append('<input type="checkbox" name="mgt301" id="mgt301" />').append('<label for="mgt301">MGT-301 Principles of Marketing </label><br>');
            $("#chkbox").append('<input type="checkbox" name="mgt503" id="mgt503" />').append('<label for="mgt503">MGT503 - Principles of Management</label><br>');
            $("#chkbox").css("margin-top", "10px");
        }else if (this.value=="MCS") {
            $("#chkbox").empty(); //For deleting all child objects.
            $("#chkbox").append('<input type="checkbox" name="cs201" id="cs201" />').append('<label for="cs201">CS-201  Introduction to Programming</label><br>');
            $("#chkbox").append('<input type="checkbox" name="cs402" id="cs402" />').append('<label for="cs402">CS402 - Theory of Automata</label><br>');
            $("#chkbox").append('<input type="checkbox" name="cs601" id="cs601" />').append('<label for="cs601">CS601 - Data Communication</label><br>');
            $("#chkbox").append('<input type="checkbox" name="eng201" id="eng201" />').append('<label for="eng201">ENG201 - Business and Technical English Writing</label><br>');
            $("#chkbox").append('<input type="checkbox" name="sta301" id="sta301" />').append('<label for="sta301">STA-301 Statistics and Probability </label><br>');
            $("#chkbox").append('<input type="checkbox" name="mth202" id="mth202" />').append('<label for="mth202">MTH202 - Discrete Mathematics</label><br>');
            $("#chkbox").css("margin-top", "10px");
        }else if (this.value=="BSCS" || this.value=="BSIT" || this.value=="BSBA") {
            $("#chkbox").empty(); //For deleting all child objects.
            $("#chkbox").append('<input type="checkbox" name="cs101" id="cs101" />').append('<label for="cs101">CS-101 Introduction to Computing</label><br>');
            $("#chkbox").append('<input type="checkbox" name="mgt101" id="mgt101" />').append('<label for="mgt101">MGT101 - Financial Accounting</label><br>');
            $("#chkbox").append('<input type="checkbox" name="eng101" id="eng101" />').append('<label for="eng101">ENG101 - English Comprehension</label><br>');
            $("#chkbox").append('<input type="checkbox" name="mth101" id="mth101" />').append('<label for="mth101">MTH101 - Calculus And Analytical Geometry</label><br>');
            $("#chkbox").append('<input type="checkbox" name="pak301" id="pak301" />').append('<label for="pak301">PAK301 - Pakistan Studies</label><br>');
            $("#chkbox").append('<input type="checkbox" name="isl201" id="isl201" />').append('<label for="isl201">ISL201 - Islamic Studies</label><br>');
            $("#chkbox").css("margin-top", "10px");
        }else{
            $("#chkbox").empty(); //For deleting all child objects.
        };
    });
});
        
