//validation change border color on empty
$(document).ready(function(){
	//First Name
	$( "#txtfirstname" ).on("focusout",function(){
		$(this).val( $(this).val().trim());
		if ($(this).val().length<2){
			$(this).css("border", "2px solid #BE4B49");
			$("#div_err_txtfirstname").empty();
			$("#div_err_txtfirstname").append('<p style="color:#BE4B49;">Please enter First Name atleast 2 Char.</p>');
		}
	});
	//Last Name
	$( "#txtlastname"  ).on("focusout",function(){
		$(this).val( $(this).val().trim());
		if ($(this).val().length<2){
			$(this).css("border", "2px solid #BE4B49");
			$("#div_err_txtlastname").empty();
			$("#div_err_txtlastname").append('<p style="color:#BE4B49;">Please enter Last Name atleast 2 Char.</p>');
		}
	});
	//email
	$( "#txtemail"  ).on("focusout",function(){
		$(this).val( $(this).val().trim());
		if ($(this).val().length<2){
			$(this).css("border", "2px solid #BE4B49");
			$("#div_err_txtemail").empty();
			$("#div_err_txtemail").append('<p style="color:#BE4B49;">Please enter an email address.</p>');
		}
	});
	//password
	$( "#txtpassword"  ).on("focusout",function(){
		$(this).val( $(this).val().trim());
		if ($(this).val().length<2){
			$(this).css("border", "2px solid #BE4B49");
			$("#div_err_txtpassword").empty();
			$("#div_err_txtpassword").append('<p style="color:#BE4B49;">Invalid password or mismatch.</p>');
		}
	});
	//repassword
	$( "#txtrepassword"  ).on("focusout",function(){
		$(this).val( $(this).val().trim());
		if ($(this).val().length<2){
			$(this).css("border", "2px solid #BE4B49");
			$("#div_err_txtrepassword").empty();
			$("#div_err_txtrepassword").append('<p style="color:#BE4B49;">Invalid password or mismatch.</p>');
		}
	});
	//description
	$( "#txtdescription"  ).on("focusout",function(){
		$(this).val( $(this).val().trim());
		if ($(this).val().length<2){
			$(this).css("border", "2px solid #BE4B49");
			$("#div_err_txtdescription").empty();
			$("#div_err_txtdescription").append('<p style="color:#BE4B49;">Please enter an email address.</p>');
		}
	});
	//select
	$( "#txtprogram").on("focusout",function(){
		//alert($(" #chkbox  input").length);
		if ($("#chkbox  input").length == 0){
			$(this).css("border", "2px solid #BE4B49");
			$("#div_err_chkbox").empty();
			$("#div_err_chkbox").append('<p style="color:#BE4B49;">Please select a valid program.</p>');
		}
	});
	//combo
	//$( " #txtpassword, #txtrepassword, #txtdescription " ).on('focusout',function(){
	//	$(this).val( $(this).val().trim());
	//	if (this.value==""){
	//		$(this).css("border", "2px solid #BE4B49");
	//	}
	//});
	//****************************************************************************************************************//
	$( "#txtfirstname, #txtlastname, #txtemail, #txtpassword, #txtrepassword, #txtdescription, #txtprogram " ).on('focus , change',function(){
		$(this).css("border", "0px");
		if (($(this).prop('id')=='txtfirstname')&&($(this).val().length<2)){
			$('#div_err_txtfirstname').empty();
		}
		if (($(this).prop('id')=='txtlastname')&&($(this).val().length<2)){
			$('#div_err_txtlastname').empty();
		}
		if (($(this).prop('id')=='txtemail')&&($(this).val().length<7	)){
			$('#div_err_txtemail').empty();
		}
		if (($(this).prop('id')=='txtpassword')&&($(this).val().length<2	)){
			$('#div_err_txtpassword').empty();
		}
		if (($(this).prop('id')=='txtrepassword')&&($(this).val().length<2	)){
			$('#div_err_txtrepassword').empty();
		}
		if (($(this).prop('id')=='txtdescription')&&($(this).val().length<2	)){
			$('#div_err_txtdescription').empty();
		}
		if ($(this).prop('id')=='txtprogram'){
			$('#div_err_chkbox').empty();
		}
	}); 
	//stop submition and add in table
	$(document).on("submit", "form", function(e){
    	e.preventDefault();
    	//alert('it works!');
    	return  false;
	});

});